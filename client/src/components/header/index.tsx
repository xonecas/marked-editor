import React from "react";
import styled from "styled-components";

export default function Header() {
  return <StyledHeader>Editor</StyledHeader>;
}

const StyledHeader = styled.div`
  grid-column: 1 / span 3;
`;
