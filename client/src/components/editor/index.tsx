import { marked } from "marked";
import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { useDarkMode } from "usehooks-ts";
import placeholderLine from "./placeholder";
import DOMPurify from "dompurify";
import Header from "../header";

export default function Editor() {
  const { toggle } = useDarkMode();
  const [placeholder, setPlaceholder] = useState("");
  const [content, setContent] = useState("");
  const [raw, setRaw] = useState("");

  const onChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    setRaw(e.target.value);
    const dirty = marked.parse(e.target.value);
    setContent(DOMPurify.sanitize(dirty, { USE_PROFILES: { html: true } }));
  };

  const onKeyDown = (e: React.KeyboardEvent<HTMLTextAreaElement>) => {
    if (e.key === "Enter" && e.ctrlKey) {
      toggle();
    }
  };

  useEffect(() => {
    if (!content) {
      setPlaceholder(placeholderLine());
    }
  }, [content]);

  return (
    <EditorContainer>
      <Header />
      <div>Sidebar</div>
      <EditorElement
        tabIndex={0}
        value={raw}
        placeholder={placeholder}
        onChange={onChange}
        onKeyDown={onKeyDown}
      />
      <EditorPreview dangerouslySetInnerHTML={{ __html: content }} />
    </EditorContainer>
  );
}

const EditorContainer = styled.div`
  display: grid;
  grid-template-rows: 100px 1fr;
  grid-template-columns: 300px 1fr 1fr;
  grid-gap: 20px;
  column-gap: 1em;
  max-width: 100%;
  height: 100%;

  color: ${(props) => props.theme.fg};
  background: ${(props) => props.theme.bg};

  > * {
    box-sizing: border-box;
    background: ${(props) => props.theme.altBg};
    border-radius: 20px;
  }
`;

const EditorElement = styled.textarea`
  margin: 0;
  display: block;
  height: 100%;
  padding: 1em;
  color: ${(props) => props.theme.fg};
  /* background: ${(props) => props.theme.bg}; */

  &,
  &:focus,
  &:focus-visible {
    outline: 0;
    border: none;
    resize: none;
    /* border-right: 4px solid ${(props) => props.theme.hl}; */
  }
`;

const EditorPreview = styled.div`
  min-height: 100%;
  padding: 0 1em;
`;
