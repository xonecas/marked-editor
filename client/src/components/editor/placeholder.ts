export function randomIntFromInterval(min: number, max: number) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

export default function placeholderLine() {
  const lines = [
    "Not bored yet, are you?",
    "This is called a 'A writer's block'...",
    "Do you also stare out the window while zoned out?",
    "I bet you read this and forgot what you meant to note down.",
    "They say you can use markdown here...",
    "Have you pressed CTRL+ENTER ?",
  ];

  return lines[randomIntFromInterval(0, lines.length - 1)];
}
