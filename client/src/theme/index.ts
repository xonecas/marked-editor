const colors = {
  BlackChocolate: "#181611",
  Isabelline: "#fbf7f4",
  Jet: "#2d2d2a",
  MetallicSeaweed: "#087e8b",
  OldLavender: "#6f5e76",
  OrangeRedCrayola: "#ff5a5f",
  PrussianBlue: "#0b3954",
};

type Theme = { bg: string; fg: string; hl: string; altBg: string };

const theme: { light: Theme; dark: Theme } = {
  dark: {
    bg: colors.BlackChocolate,
    altBg: colors.Jet,
    fg: colors.Isabelline,
    hl: colors.OrangeRedCrayola,
  },
  light: {
    bg: colors.Isabelline,
    altBg: colors.BlackChocolate,
    fg: colors.Jet,
    hl: colors.PrussianBlue,
  },
};

export default theme;
