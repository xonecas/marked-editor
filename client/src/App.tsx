import React from "react";
import { RecoilRoot } from "recoil";
import { ThemeProvider } from "styled-components";
import { useDarkMode } from "usehooks-ts";
import theme from "./theme";
import Editor from "./components/editor";

function App() {
  const { isDarkMode } = useDarkMode();

  return (
    <RecoilRoot>
      <ThemeProvider theme={isDarkMode ? theme.dark : theme.light}>
        <Editor />
      </ThemeProvider>
    </RecoilRoot>
  );
}

export default App;
